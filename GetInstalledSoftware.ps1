<#
.Synopsis
    This code will all you to extract information about products installed on a computer
    Get ALL software installed regardless of whether it's 64bit or 32bit Windows
    c:\> $allInstalledSoftware = [Software.Installed]::GetAll();

    Get ALL software installed on a remote machine
    c:\> $remotePcSoftware = [Software.Installed]::GetAll("computer-name");

    Get a particular package on the local computer.
    c:\> $chrome = [Software.Installed]::GetPackage("Google Chrome");

    Get output in table or whatever format just like anything else
    c:\> $chrome | Format-Table * -AutoSize;
    c:\> $allInstalledSoftware | Out-GridView;

    Extract the version
    c:\> $chromeVersion = $chrome | Select-Object -ExpandProperty Version;
    Shorthand
    c:\> $chromeVersion = ($chrome).Vesion;
    
.NOTES
    Author  : Richard Abel - rabel001@gitlab.com & rabeljr@github.com (least used)
    Date    : 10/16/2019
    Vers    : 1.0
	URL     : https://gitlab.com/rabel001
#>
	
Add-Type @"
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Software
{
	public class Installed
	{
		public class Package
		{
			public string Name { get; set; }
			public string Version { get; set; }
			public string InstallLocation { get; set; }
			public string UninstallString { get; set; }
			public string HelpLink { get; set; }
		}

		private static string getString(string str)
		{
			return (string.IsNullOrEmpty(str)) ? "" : str;
		}

		private static List<Package> getList(RegistryKey r)
		{
			return r.GetSubKeyNames().Select((c) =>
			{
				RegistryKey rk = r.OpenSubKey(c);
				string displayName = (string)rk.GetValue("DisplayName");
				string displayVersion = (string)rk.GetValue("DisplayVersion");
				string uninstallString = (string)rk.GetValue("UninstallString");
				string helpLink = (string)rk.GetValue("HelpLink");
				string installLocation = (string)rk.GetValue("InstallSource");
				if(string.IsNullOrEmpty(installLocation))
				{
					installLocation = (string)rk.GetValue("InstallLocation");
				}
				return new Package
				{
					Name = getString(displayName),
					Version = getString(displayVersion),
					InstallLocation = getString(installLocation),
					UninstallString = getString(uninstallString),
					HelpLink = getString(helpLink)
				};
			}).ToList<Package>();
		}
		public static List<Package> GetAll(string computerName = "")
		{
			string uninstallKey32 = @"Software\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall";
			string uninstallKey64 = @"Software\Microsoft\Windows\CurrentVersion\Uninstall";
			List<Package> subKeys;
			RegistryKey regKey32;
			RegistryKey regKey64;
			List<Package> subKey64;
			if(!string.IsNullOrEmpty(computerName))
			{
				RegistryKey regKeyHive = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, computerName);
				regKey32 = regKeyHive.OpenSubKey(uninstallKey32);
				subKeys = getList(regKey32);
				if(Environment.Is64BitOperatingSystem)
				{
					regKey64 = regKeyHive.OpenSubKey(uninstallKey64);
					subKey64 = getList(regKey64);
					subKeys = subKeys.Union(subKey64).ToList();
				}
			}
			else
			{
				regKey32 = Registry.LocalMachine.OpenSubKey(uninstallKey32);
				subKeys = getList(regKey32);
				if(Environment.Is64BitOperatingSystem)
				{
					regKey64 = Registry.LocalMachine.OpenSubKey(uninstallKey64);
					subKey64 = getList(regKey64);
					subKeys = subKeys.Union(subKey64).ToList();
				}
			}
			return subKeys;
		}
		public static List<Package> GetPackage(string pkg, string computerName = "")
		{
			pkg = pkg.ToLower();
			List<Package> allPackages = (String.IsNullOrEmpty(computerName)) ? GetAll() : GetAll(computerName);
			List<Package> packages = allPackages.Where(x => x.Name.ToLower().Contains(pkg)).ToList();
			if(!packages.Any())
			{
				packages = allPackages.Where(x => x.InstallLocation.ToLower().Contains(pkg)).ToList();
				if(!packages.Any())
				{
					packages = allPackages.Where(x => x.HelpLink.ToLower().Contains(pkg)).ToList();
				}
			}
			return packages;
		}
	}
}
"@

#####################
# Example functions #
#####################
function Get-Software() {
	return [Software.Installed]::GetAll();
}
function Get-Package($pkg) {
	return [Software.Installed]::GetPackage($pkg);
}