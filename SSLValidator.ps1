$SSLValidator = @'
using System.Collections.Generic;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

public static class SSLValidator
{
	private static Stack<RemoteCertificateValidationCallback> funcs = new Stack<RemoteCertificateValidationCallback>();

	private static bool OnValidateCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
	{
		return true;
	}

	public static void OverrideValidation()
	{
		funcs.Push(ServicePointManager.ServerCertificateValidationCallback);
		ServicePointManager.ServerCertificateValidationCallback = OnValidateCertificate;
	}

	public static void RestoreValidation()
	{
		if (funcs.Count > 0) {
			ServicePointManager.ServerCertificateValidationCallback = funcs.Pop();
		}
	}
}
'@

function Set-SSLValidator
{
	<#
	.Synopsis
	  This script can be used to disable and enable SSL certificate validation.
	  This script was created to help eleviate issuse with Invoke-WebRequest
	  and Invoke-RestMethod. Since these native commands do not have a parameter
	  available for trusting amy certificate, this one does.
	  
	  This will help when the following does not:
	  [System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
	  
	  It should also help if the follow does not:
	  $webRequest = [System.Net.WebRequest]::Create($url)
	  $webRequest.ServerCertificateValidationCallback = {$true}

	.Description
	  Provides the following calls:
	  This command sets all web request SSL validations to ignore errors:
	  Set-SSLValidator -EnableOverride
	  This  command sets all web request SSL validations to check for errors:
	  Set-SSLValidator -DisableOverride

	.NOTES

	  Author  : Richard Abel (rabel001@gitlab.com & rabeljr@github.com - lesser)
	  Date    : 08/20/2016
	  Vers    : 1.0
	  URL     : https://gitlab.com/rabel001
	  
	.PARAMETER EnableOverride
	  Sets all web request SSL validations to ignore errors
	  
	.PARAMETER DisableOverride
	  Sets all web request SSL validations to check for errors

	.LINK
	  https://gitlab.com/rabel001

	.EXAMPLE
	  . .\SSLValidator.ps1 #imports everything from the script including the function
	  Set-SSLValidator -EnableOverride
	  
	.EXAMPLE
	  . .\SSLValidator.ps1 #imports everything from the script including the function
	  Set-SSLValidator -DisableOverride
	#>

	#[CmdletBinding(DefaultParameterSetName='ByDisabled')]
	param(
		[Parameter(Mandatory = $true, ParameterSetName = 'ByEnabled', Position=0)]
		[switch]$EnableOverride,
		[Parameter(Mandatory = $true, ParameterSetName = 'ByDisabled', Position=0)]
		[switch]$DisableOverride
	)
	[void][Reflection.Assembly]::LoadWithPartialName("System");
	[void][Reflection.Assembly]::LoadWithPartialName("System.Core");
	Add-Type -ReferencedAssemblies @("System", "System.Core") -TypeDefinition $SSLValidator
	if($EnableOverride) {
		[SSLValidator]::OverrideValidation()
		Write-Host "Override successfully enabled. SSL errors will now be ignored." -ForegroundColor Green;
	} elseif($DisableOverride) {
		[SSLValidator]::RestoreValidation()
		Write-Host "Override successfully disabled. SSL errors will now be validated." -ForegroundColor Green;
	}
}