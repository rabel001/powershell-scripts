<#
.Synopsis
    This is just a reference as for how dynamic link libraries can be loaded along with CSharp code.
    In this example there isn't any code, just library imports. This is usually this biggest issue
    when using Add-Type with a string of CSharp code.
   
.Notes
    Author  : Richard Abel (rabel001@gitlab.com & rabeljr@github.com - lesser)
    Date    : 10/16/2017
    Vers    : 1.0
    URL     : https://gitlab.com/rabel001
#>

$code = @"
    using System;
    using System.DirectoryServices.AccountManagement;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;
    using System.Runtime.InteropServices;
    using System.Text.RegularExpressions;
"@


[Reflection.Assembly]::LoadWithPartialName("System") 
[Reflection.Assembly]::LoadWithPartialName("System.Core")
[Reflection.Assembly]::LoadWithPartialName("System.DirectoryServices.AccountManagement")
[Reflection.Assembly]::LoadWithPartialName("System.IO") 
[Reflection.Assembly]::LoadWithPartialName("System.Linq") 
[Reflection.Assembly]::LoadWithPartialName("System.Xml.Linq") 
[Reflection.Assembly]::LoadWithPartialName("System.Runtime.InteropServices") 
[Reflection.Assembly]::LoadWithPartialName("System.Text.RegularExpressions") 

$refs = @("System", "System.DirectoryServices.AccountManagement", "System.Core", "System.IO", "System.Linq", "System.Xml.Linq", "System.Runtime.InteropServices", "System.Text.RegularExpressions")

Add-Type -ReferencedAssemblies $refs -TypeDefinition $code